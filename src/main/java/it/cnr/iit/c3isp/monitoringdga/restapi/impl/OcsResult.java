package it.cnr.iit.c3isp.monitoringdga.restapi.impl;

public class OcsResult {

	private String DGAname = "";
	private String timestamp = "";
	private String zone = "";

	public String getDGAname() {
		return DGAname;
	}

	public void setDGAname(String dGAname) {
		DGAname = dGAname;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

}

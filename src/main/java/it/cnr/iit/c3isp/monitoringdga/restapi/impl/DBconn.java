package it.cnr.iit.c3isp.monitoringdga.restapi.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBconn {

	Statement stmt;
	Connection conn;

	/**
	 * Getting OS name
	 */
	private String OS = System.getProperty("os.name").toLowerCase();

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger log = LoggerFactory.getLogger(DGAServiceImplementation.class);

	// Constructor
	public DBconn() {
		SetConnection();
	}

	// This method creates a connection to the DB
	private void SetConnection() {
		System.setProperty("https.protocols", "TLSv1.2");
		LoadPropertiesFile properties = new LoadPropertiesFile();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			/**
			 * Driving DB connection depending on the depoyment type
			 */
			if ((OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0)) // if the API runs on IAI
																								// machine
			{
				log.error("> Connecting DB on IAI server");
//				conn = DriverManager.getConnection(properties.getProperty("dbIai"));
				conn = DriverManager.getConnection(
						"jdbc:mysql://" + properties.getProperty("database") + ":3306/dga_list",
						properties.getProperty("dbuser"), properties.getProperty("dbpassword"));

			} else if ((OS.indexOf("mac") >= 0)) // if the API is running on Mac OS
			{
				log.error("> Connecting DB on IAI server on mac os");
//				conn = DriverManager.getConnection(properties.getProperty("dbOsx"));
				conn = DriverManager.getConnection(
						"jdbc:mysql://" + properties.getProperty("database") + ":3306/dga_list",
						properties.getProperty("dbuser_osx"), properties.getProperty("dbpassword_osx"));
			}

			stmt = conn.createStatement();
			log.error("> DB Connected");

		} catch (Exception e) {
			log.error("Got an exception!");
			e.printStackTrace();
		}
	}

	// This method returns a connection to the DB
	public Statement getStatement() {
		return stmt;
	}

	public Connection getConnection() {
		return conn;
	}

	public void closeConnectionDB() {
		try {
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}

}

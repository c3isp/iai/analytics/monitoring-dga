package it.cnr.iit.c3isp.monitoringdga.restapi.impl;

public class CEFcleaned {

	private String CEF;
	private String url;
	private String timestamp;
	private String zone;

	public CEFcleaned(String cef, String url, String ts, String zone) {
		this.CEF = cef;
		this.url = url;
		this.timestamp = ts;
		this.zone = zone;
	}

	public String getCEF() {
		return CEF;
	}

	public void setCEF(String cEF) {
		CEF = cEF;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

}

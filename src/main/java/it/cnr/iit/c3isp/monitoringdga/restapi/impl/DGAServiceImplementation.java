/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.c3isp.monitoringdga.restapi.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.github.jcustenborder.cef.CEFParser;
import com.github.jcustenborder.cef.CEFParserFactory;
import com.github.jcustenborder.cef.Message;
import com.google.common.net.InternetDomainName;

@Component
public class DGAServiceImplementation {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */

	private final static Logger log = LoggerFactory.getLogger(DGAServiceImplementation.class);

	/**
	 * To load form the properties file
	 */
	private LoadPropertiesFile properties = new LoadPropertiesFile();

	boolean SHOW_LOG = false;

	/**
	 * Getting OS name
	 */
	private String OS = System.getProperty("os.name").toLowerCase();

	DBconn state = new DBconn();
	Statement stmt = state.getStatement();

	/**
	 * This is a POST method that takes as input a JSON file containing the URI to
	 * the CEF file stored in the VDL. The CEF file contains DNS query log and says
	 * if some queries are DGAs or not. In particular, it uses two SOURCES, which is
	 * a public one and that one coming from the ISP, to match the DSAs
	 * 
	 * @RequestParam file it is the CEF file containing the DNS requests
	 * @return just information about executing, i.e., 200
	 */

	// @RequestMapping(method = RequestMethod.POST, value = "/matchDGA")

	public String matchDGA(String pathVDLParam) {

		log.info("matchDGA has been called");

		if (pathVDLParam == null || pathVDLParam.isEmpty()) {
			log.info("Error: VDL path is NULL");
//			return new AnalyticResult("ERROR", "VDL path is missing");
			return "VDL path is missing";
		}

		String resultFilename = "";
		log.info("CEF input from VDL: " + pathVDLParam);

		try {
			long startTime = new Date().getTime();
			String tempMSGString, tempENDString, tempDTZString;
			String CEFprefix;
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(pathVDLParam)));
			CEFParser f = CEFParserFactory.create();
			String CEFwithOnlyPrefixMsg = "";

			ArrayList<CEFcleaned> dgaList = new ArrayList<CEFcleaned>();

			String strLine = "";
			while ((strLine = br.readLine()) != null) {
				log.info("strLine: " + strLine);

				Message message = f.parse(strLine);
				tempMSGString = message.extensions().get("msg");
				tempENDString = message.extensions().get("end");
				tempDTZString = message.extensions().get("dtz");
				log.info("msg: " + tempMSGString);
				log.info("end: " + tempENDString);
				log.info("dtz: " + tempDTZString);

				CEFprefix = "CEF:" + message.cefVersion() + "|" + message.deviceVendor() + "|" + message.deviceProduct()
						+ "|" + message.deviceVersion() + "|" + message.deviceEventClassId() + "|" + message.name()
						+ "|" + message.severity() + "|";
				log.info(CEFprefix);

				String url = tempMSGString.split(" ")[0];
				url = getDomainName(url);
				log.info("> url cleaned: " + url);

				CEFwithOnlyPrefixMsg = CEFprefix + "msg=" + url;
				dgaList.add(new CEFcleaned(CEFwithOnlyPrefixMsg, url, tempENDString, tempDTZString));
			}

			br.close();

			MatchDGA mDGA = new MatchDGA(dgaList);
			resultFilename = mDGA.matchDgaWithDB();

			log.info("Processing " + resultFilename + " to check if it is empty");

			br = new BufferedReader(new FileReader(resultFilename));
			if (br.readLine() == null) {
				PrintWriter writer = new PrintWriter(resultFilename, "UTF-8");
				writer.println("DGAs not found");
				log.info("DGAs NOT found!!!");
				writer.close();
			}
			br.close();

//			File resultFile = new File(resultFilename);
//			List<byte[]> resultList = new ArrayList<byte[]>();
//			resultList.add(FileUtils.readFileToByteArray(resultFile));
//			AnalyticResult analyticResult = new AnalyticResult();
//			analyticResult.setStatus("SUCCESS");
//			analyticResult.setResultArray(resultList);
//			new File(resultFilename).delete();

			long endTime = new Date().getTime();
			long timeElapsed = endTime - startTime;
			log.info("Execution time: " + getDurationBreakdown(timeElapsed));

			return resultFilename;
		} catch (Exception e) {
			e.printStackTrace();
			return "An error occurred during the analytic execution";
//			return new AnalyticResult("ERROR", "An error occurred during the analytic execution");
		}

	}

	/**
	 * This is a POST method that takes as input a JSON file containing the URI to
	 * the CEF file stored in the VDL. The CEF file contains DNS query log and says
	 * if some queries are DGAs or not. In particular, it a particular algorithm the
	 * check if the request was a DGA. For all DGAs found, the algorithm creates a
	 * CEF file that then can be shared with C3ISP with the /shareDGA api
	 * 
	 * @RequestParam file it is the CEF file containing the DNS requests
	 * @return just information about executing, i.e., 200
	 */
	// @RequestMapping(value="/detectDGA", method=RequestMethod.POST, consumes =
	// MediaType.APPLICATION_JSON_VALUE, produces =
	// MediaType.APPLICATION_JSON_VALUE)

	public String detectDGA(String pathVDLParam) {
		log.info("DetectDGA has been called");

		if (pathVDLParam == null || pathVDLParam.isEmpty()) {
			log.info("Error: VDL path is NULL");
			return "VDL path is missing";
		}

		log.info("CEF input from VDL: " + pathVDLParam);

		try {
			long startTime = new Date().getTime();

			String tempMSGString, tempENDString, tempDTZString; // the temp CEF string from the file

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(pathVDLParam)));
			CEFParser f = CEFParserFactory.create();

			String dgaToScan = "";
			// if ((OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") >
			// 0 )) // if the API runs on IAI machine
			// dgaToScan =
			// properties.getProperty("cef_loaded_dlb_stored")+"dga-to-scan_"+new
			// SimpleDateFormat("yyyyMMdd_HHmmss'.txt'").format(new Date());
			// else if ((OS.indexOf("mac") >= 0)) // if the API is running on Mac OS
			// dgaToScan =
			// properties.getProperty("cef_loaded_dlb_stored_osx")+"dga-to-scan_"+new
			// SimpleDateFormat("yyyyMMdd_HHmmss'.txt'").format(new Date());

			dgaToScan = new File(pathVDLParam).getAbsoluteFile().getParentFile().getAbsolutePath() + "/dga-to-scan_"
					+ new SimpleDateFormat("yyyyMMdd_HHmmss'.txt'").format(new Date());
			PrintWriter writer = new PrintWriter(dgaToScan, "UTF-8");
			String CEFprefix = "";

			String strLine = "";
			while ((strLine = br.readLine()) != null) {
				log.info("strLine: " + strLine);

				Message message = f.parse(strLine);
				tempMSGString = message.extensions().get("msg");
				tempENDString = message.extensions().get("end");
				tempDTZString = message.extensions().get("dtz");
				log.info("msg: " + tempMSGString);
				log.info("end: " + tempENDString);
				log.info("dtz: " + tempDTZString);

				CEFprefix = "CEF:" + message.cefVersion() + "|" + message.deviceVendor() + "|" + message.deviceProduct()
						+ "|" + message.deviceVersion() + "|" + message.deviceEventClassId() + "|" + message.name()
						+ "|" + message.severity() + "|";
				log.info(CEFprefix);

				String url = tempMSGString.split(" ")[0];

				url = getDomainName(url);
				log.info("url cleaned: " + url);
				writer.println(CEFprefix + "," + url + "," + tempENDString + "," + tempDTZString);
			}
			writer.close();
			br.close();

			dgaToScan = dgaToScan.replace(" ", "\\ ");
			log.info("dgaToScan: " + dgaToScan);

			/**
			 * The following is to execute the python script to detect DGA This new
			 * File(properties.getProperty("dga_detector_local") is to move the path where
			 * the dga_detector.py is located in the file system
			 */

			BufferedReader[] stdArray = invokePythonDetectDga(dgaToScan);
			BufferedReader stdInput = stdArray[0];
			BufferedReader stdError = stdArray[1];

			String dgafoundfile = "";
			log.info("Standard output of the command:");
			while ((strLine = stdInput.readLine()) != null) {
				log.info(strLine);

				/**
				 * We take the filename created by the script where there are found DGA from an
				 * example string File dga_found_20180719_180031.txt created
				 */
				if (strLine.startsWith("File")) {
					dgafoundfile = strLine.split(" ")[1];
				}
			}

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();

			if ((OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0)) // if the API runs on
																								// IAI machine
			{
				dgafoundfile = properties.getProperty("dga_detector_local") + dateFormat.format(date) + "/"
						+ dgafoundfile;
			} else if ((OS.indexOf("mac") >= 0)) // if the API is running on Mac OS
			{
				dgafoundfile = properties.getProperty("dga_detector_local_osx") + dateFormat.format(date) + "/"
						+ dgafoundfile;

			}

			log.info("Full path DGA found file: " + dgafoundfile);

			log.info("Standard error of the command (if any):");
			while ((strLine = stdError.readLine()) != null) {
				log.info(strLine);
			}

			log.info("Processing " + dgafoundfile + " to check if it is empty");

			br = new BufferedReader(new FileReader(dgafoundfile));
			if (br.readLine() == null) {
				writer = new PrintWriter(dgafoundfile, "UTF-8");
				writer.println("DGAs not found");
				log.info("DGAs NOT found!!!");
				writer.close();
			}
			br.close();
			stdInput.close();
			stdError.close();

			long endTime = new Date().getTime();
			long timeElapsed = endTime - startTime;
			log.info("Execution time: " + getDurationBreakdown(timeElapsed));
			return dgafoundfile;

		} catch (Exception e) {
			e.printStackTrace();
			return "An error occurred during the analytic execution";
		}

	}

	private BufferedReader[] invokePythonDetectDga(String dgaToScan) throws IOException {
		Process p = null;
		if ((OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0)) // if the API runs on
																							// IAI machine
		{
			log.info("Eseguo: " + properties.getProperty("pythonlocal") + "python3 "
					+ properties.getProperty("dga_detector_local") + "dga_detector.py -f " + dgaToScan);
			p = Runtime.getRuntime().exec(
					properties.getProperty("pythonlocal") + "python3 dga_detector.py -f " + dgaToScan, null,
					new File(properties.getProperty("dga_detector_local")));
		} else if ((OS.indexOf("mac") >= 0)) // if the API is running on Mac OS
		{
			log.info("Eseguo: " + properties.getProperty("pythonlocal_osx") + "python3 "
					+ properties.getProperty("dga_detector_local_osx") + "dga_detector.py -f " + dgaToScan);
			p = Runtime.getRuntime().exec(
					properties.getProperty("pythonlocal_osx") + "python3 dga_detector.py -f " + dgaToScan, null,
					new File(properties.getProperty("dga_detector_local_osx")));
		}

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		BufferedReader[] brArray = { stdInput, stdError };

		return brArray;

	}

	/**
	 * This method is to get only the name of the web site. For instance, from
	 * p49-keyvalueservice.icloud.com, it returns icloud.com
	 * 
	 * @param url
	 * @return
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 */
	public String getDomainName(String url) throws URISyntaxException, MalformedURLException, IllegalStateException {

		List<String> allowedSites = Arrays.asList("dropbox", "ilnostrocnr", "crazyegg.com", "akamaihd.net", "adobedtm",
				"yahoo", "research");

		/**
		 * Just a simple check to avoid the allowed url may be detected as DGAs For
		 * example: edge-block-www.traffic-test.dropbox-dns.com is detected as DGA
		 */
		for (String str : allowedSites) {
			if (url.contains(str)) {

				log.info("Url: " + url + " is in the allowedList");
				log.info("Url: " + url + " changed to www.c3isp.eu");
				return "www.c3isp.eu";
			}
		}

		/**
		 * Just a check on malformed url. In this situation we return a good url :)
		 */
		if ((url.equalsIgnoreCase("com")) || (url.equalsIgnoreCase("")) || (url.equalsIgnoreCase("."))
				|| (url.contains("in-addr.arpa")))
			return "www.c3isp.eu";

		/**
		 * Here we add the protocol just to avoid the malformed error
		 */

		String urlString = "http://" + url;
		URL urll = new URL(urlString);
		String host = urll.getHost();

		/**
		 * Additional checks on malformed url. In this situation we return a good url :)
		 */
		host = host.replaceAll("_", "");
		host = host.replaceAll("VERSION.BIND", "www.c3isp.eu");
		host = host.replaceAll("version.bind", "www.c3isp.eu");
		host = host.replaceAll(". IN ANY +E ", "www.c3isp.eu");
		host = host.replaceAll("local", "www.c3isp.eu");

		InternetDomainName name = InternetDomainName.from(host).topPrivateDomain();
		return "" + name;
		// System.out.println(urlString);
		// System.out.println(host);
		// System.out.println(name);

	}

	/**
	 * Convert a millisecond duration to a string format
	 * 
	 * @param millis A duration to convert to a string form
	 * @return A string of the form "X Days Y Hours Z Minutes A Seconds".
	 */
	public String getDurationBreakdown(long millis) {
		if (millis < 0) {
			throw new IllegalArgumentException("Duration must be greater than zero!");
		}

		long days = TimeUnit.MILLISECONDS.toDays(millis);
		millis -= TimeUnit.DAYS.toMillis(days);
		long hours = TimeUnit.MILLISECONDS.toHours(millis);
		millis -= TimeUnit.HOURS.toMillis(hours);
		long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
		millis -= TimeUnit.MINUTES.toMillis(minutes);
		long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

		StringBuilder sb = new StringBuilder(64);
		sb.append(days);
		sb.append(" Days ");
		sb.append(hours);
		sb.append(" Hours ");
		sb.append(minutes);
		sb.append(" Minutes ");
		sb.append(seconds);
		sb.append(" Seconds");

		return (sb.toString());
	}

}

package it.cnr.iit.c3isp.monitoringdga.controller;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.c3isp.monitordga.async.AsyncAnalyticExecution;
import it.cnr.iit.c3isp.monitoringdga.restapi.impl.Path;
import it.cnr.iit.common.analytic.AnalyticManager;
import it.cnr.iit.common.analytic.AnalyticResult;

@ApiModel(value = "DgaAnalytics", description = "Dga Analytic for dga detection")
@RestController
@RequestMapping("/v1")
public class MonitoringDgaController {

	@Autowired
	AsyncAnalyticExecution asyncAnalyticExecution;

	private AnalyticManager<String> analyticManager = new AnalyticManager<String>();

	private final static Logger log = LoggerFactory.getLogger(MonitoringDgaController.class);

	public MonitoringDgaController() {
		// TODO Auto-generated constructor stub
	}

	@ApiOperation(value = "Run the shareDGA Anlytic that takes domain-names and check if they are DGA using a public source of known DGAs", nickname = "matchDGA")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Server response", response = String.class),
			@ApiResponse(code = 400, message = "Bad request", response = String.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = String.class),
			@ApiResponse(code = 404, message = "Not found", response = String.class),
			@ApiResponse(code = 500, message = "Error for HTTPS call trustAnchors", response = String.class) })
	@RequestMapping(value = "/matchDGA/{sessionId}", consumes = { "application/json" }, produces = {
			"application/json" }, method = RequestMethod.POST)
	public AnalyticResult matchDGA(@RequestBody Path pathVDLParam, @PathVariable String sessionId) {
		try {
			Future<String> future = asyncAnalyticExecution.executeWithResult(pathVDLParam.getPath(), "matchDga");
			analyticManager.getFutureMap().putFuture(sessionId, future);
			String result = future.get();

			byte[] fileContent = Files.readAllBytes(Paths.get(result));
			AnalyticResult analyticResult = new AnalyticResult();
			analyticResult.setStatus("ok");
			analyticResult.addToResultArray(fileContent);

			Paths.get(result).toFile().delete();

			analyticManager.getFutureMap().removeFuture(sessionId);

			return analyticResult;
		} catch (CancellationException ce) {
			return new AnalyticResult("error", "It was impossible to complete the analytic with sessionId " + sessionId
					+ " because it has been stopped");
		} catch (Exception e) {
			e.printStackTrace();
			return new AnalyticResult("error", e.getMessage());
		}
	}

	@ApiOperation(value = "Run the detectDGA Anlytic that takes domain-names and check if they are DGA using a third-party algorithm", nickname = "detectDGA")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Server response", response = String.class),
			@ApiResponse(code = 400, message = "Bad request", response = String.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = String.class),
			@ApiResponse(code = 404, message = "Not found", response = String.class),
			@ApiResponse(code = 500, message = "Error for HTTPS call trustAnchors", response = String.class) })
	@RequestMapping(value = "/detectDGA/{sessionId}", consumes = { "application/json" }, produces = {
			"application/json" }, method = RequestMethod.POST)
	// public ResponseEntity<String> detectDGA(@RequestBody Path pathVDLParam)
	public AnalyticResult detectDGA(@RequestBody Path pathVDLParam, @PathVariable String sessionId) {
		try {
			Future<String> future = asyncAnalyticExecution.executeWithResult(pathVDLParam.getPath(), "detectDga");
			analyticManager.getFutureMap().putFuture(sessionId, future);
			String result = future.get();

			byte[] fileContent = Files.readAllBytes(Paths.get(result));
			AnalyticResult analyticResult = new AnalyticResult();
			analyticResult.setStatus("ok");
			analyticResult.addToResultArray(fileContent);

			Paths.get(result).toFile().delete();

			analyticManager.getFutureMap().removeFuture(sessionId);

			return analyticResult;
		} catch (CancellationException ce) {
			return new AnalyticResult("error", "It was impossible to complete the analytic with sessionId " + sessionId
					+ " because it has been stopped");
		} catch (Exception e) {
			e.printStackTrace();
			return new AnalyticResult("error", e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/stopAnalytic/{sessionId}")
	public String stopAnalytic(@PathVariable String sessionId) {
		log.info("Stopping analytic with sessionId=" + sessionId);
		return analyticManager.stopAnalytic(sessionId);
	}

}

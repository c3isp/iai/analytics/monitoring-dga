package it.cnr.iit.c3isp.monitoringdga.restapi.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadPropertiesFile {

	Properties prop = new Properties();

	public LoadPropertiesFile() {

		InputStream input = null;

		try {
			input = LoadPropertiesFile.class.getClassLoader().getResourceAsStream("config.properties");
			// input = new FileInputStream(stream);

			// load a properties file
			prop.load(input);

			// get the property value and print it out
//			System.out.println(prop.getProperty("database"));
//			System.out.println(prop.getProperty("dbuser"));
//			System.out.println(prop.getProperty("dbpassword"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String getProperty(String proper) {
		return prop.getProperty(proper);
	}
}

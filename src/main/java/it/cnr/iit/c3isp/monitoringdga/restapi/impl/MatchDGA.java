package it.cnr.iit.c3isp.monitoringdga.restapi.impl;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MatchDGA {

	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(DGAServiceImplementation.class);

	ArrayList<CEFcleaned> dgaListToMatch;

	static Statement stmt;
	static ResultSet rs;
	static DBconn state;

	public MatchDGA(ArrayList<CEFcleaned> dgalist) {
		dgaListToMatch = dgalist;
		state = new DBconn();
		stmt = state.getStatement();
	}

	/**
	 * This method checks DGA from the table populated by DGAs discovered by ISPs
	 */
	public ArrayList<OcsResult> matchDgaFromISP() {
		/**
		 * Array JSON to return in the API
		 */
		ArrayList<OcsResult> jsonResponse = new ArrayList<OcsResult>();
		OcsResult result = new OcsResult();
		String queryCheck = "SELECT count(*) from dga_known WHERE url = ?";
		PreparedStatement ps;
		ResultSet resultSet;

		String url, cef, timestamp, zone = "";

		try {
			LOGGER.info("Checking DGA from ISP sources...");
			for (int i = 0; i < dgaListToMatch.size(); i++) {
				url = (dgaListToMatch.get(i)).getUrl();
				cef = (dgaListToMatch.get(i)).getCEF();
				timestamp = (dgaListToMatch.get(i)).getTimestamp();
				zone = (dgaListToMatch.get(i)).getZone();

				timestamp = (dgaListToMatch.get(i)).getTimestamp();
				zone = (dgaListToMatch.get(i)).getZone();
				queryCheck = "SELECT count(*) from dga_known_isp WHERE url = \'" + url + "\'";
				ps = state.getConnection().prepareStatement(queryCheck);

				LOGGER.info("Executing query... #" + i);
				LOGGER.info("query: " + queryCheck);

				resultSet = ps.executeQuery();

				int count;
				resultSet.next();
				count = resultSet.getInt(1);
				/**
				 * If count > 0 it means that the DGA has been found in the database
				 */
				if (count > 0) {
					count = resultSet.getInt(1);
					LOGGER.info(url + " found in the DGA database");
					LOGGER.info("Corrisponding CEF: " + cef);
					LOGGER.info("count: " + count);
					/**
					 * We add the result to the object
					 */
					result.setDGAname(url);
					result.setTimestamp(timestamp);
					result.setZone(zone);

					/**
					 * We add the object to the arraylist to build the JSON file
					 */
					jsonResponse.add(result);
				} else {
					count = 0;
					LOGGER.info(url + " NOT found in the DGA database");
					LOGGER.info("Corrisponding CEF: " + cef);
					LOGGER.info("count: " + count);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setDGAname(e.getMessage());
			jsonResponse.add(result);
			return jsonResponse;
		}

		if (jsonResponse.isEmpty()) {
			result.setDGAname("Non è stato trovato Nessun DGA");
			jsonResponse.add(result);
		}

		return jsonResponse;
	}

	/**
	 * THis is for C3ISP analytic
	 * 
	 * @return
	 */
	public String matchDgaWithDB() {

		String queryCheck = "SELECT count(*) from dga_known WHERE url = ?";
		PreparedStatement ps;
		ResultSet resultSet;
		String ext = "cef";

		/**
		 * We temporary save the file into /tmp/
		 */
		String matchDGAfilename = "/tmp/" + String.format("%s.%s", RandomStringUtils.randomAlphanumeric(8), ext);
//		File file = new File(dir, name);

		PrintWriter writer = null;

		try {
			writer = new PrintWriter(matchDGAfilename, "UTF-8");
			LOGGER.info("Checking DGA from public site source...");

			String url, cef, timestamp, zone, dgafound = "";

			for (int i = 0; i < dgaListToMatch.size(); i++) {
				url = (dgaListToMatch.get(i)).getUrl();
				cef = (dgaListToMatch.get(i)).getCEF();
				timestamp = (dgaListToMatch.get(i)).getTimestamp();
				zone = (dgaListToMatch.get(i)).getZone();

				queryCheck = "SELECT count(*) from dga_known WHERE url = \'" + url + "\'";
				ps = state.getConnection().prepareStatement(queryCheck);

				LOGGER.info("Executing query... #" + i);
				LOGGER.info("query: " + queryCheck);

				resultSet = ps.executeQuery();

				int count;
				resultSet.next();
				count = resultSet.getInt(1);
				/**
				 * If count > 0 it means that the DGA has been found in the database
				 */
				if (count > 0) {
					count = resultSet.getInt(1);
					LOGGER.info(url + " found in the DGA database");
					LOGGER.info("Corrisponding CEF: " + cef);
					LOGGER.info("count: " + count);

					/**
					 * We here write the CEF line in the file
					 * 
					 */

					dgafound = cef + " end=" + timestamp + " dtz=" + zone;
					writer.println(dgafound);
					LOGGER.info("A match DGA has been found: " + dgafound);
				} else {
					count = 0;
					LOGGER.info(url + " NOT found in the DGA database");
					LOGGER.info("Corrisponding CEF: " + cef);
					LOGGER.info("count: " + count);
				}
			}
		} catch (SQLException | FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			writer.println(e.getMessage());
			return matchDGAfilename;
		}

		writer.close();

//		/**
//		 * Here we check if any DGA was not found
//		 * So we check if the file is empty
//		 */
//		File file = new File(matchDGAfilename);
//		boolean empty = file.length() == 0;	
//		if (empty)
//		{
//			writer.println("DGAs not found");
//			LOGGER.info("DGAs not found!!!");
//		}
//		writer.close();

		return matchDGAfilename;
	}

	/**
	 * This method checks DGA from the table populated by the public site source
	 * used by the OCS api
	 */
	public ArrayList<OcsResult> matchDgaWithDB_ocs() {
		/**
		 * Array JSON to return in the API
		 */
		ArrayList<OcsResult> jsonResponse = new ArrayList<OcsResult>();
		OcsResult result = new OcsResult();

		String queryCheck = "SELECT count(*) from dga_known WHERE url = ?";
		PreparedStatement ps;
		ResultSet resultSet;

		try {
			LOGGER.info("Checking DGA from public site source...");

			String url, cef, timestamp, zone = "";
			for (int i = 0; i < dgaListToMatch.size(); i++) {
				url = (dgaListToMatch.get(i)).getUrl();
				cef = (dgaListToMatch.get(i)).getCEF();
				timestamp = (dgaListToMatch.get(i)).getTimestamp();
				zone = (dgaListToMatch.get(i)).getZone();

				queryCheck = "SELECT count(*) from dga_known WHERE url = \'" + url + "\'";
				ps = state.getConnection().prepareStatement(queryCheck);

				LOGGER.info("Executing query... #" + i);
				LOGGER.info("query: " + queryCheck);

				resultSet = ps.executeQuery();

				int count;
				resultSet.next();
				count = resultSet.getInt(1);
				/**
				 * If count > 0 it means that the DGA has been found in the database
				 */
				if (count > 0) {
					count = resultSet.getInt(1);
					LOGGER.info(url + " found in the DGA database");
					LOGGER.info("Corrisponding CEF: " + cef);
					LOGGER.info("count: " + count);

					/**
					 * We add the result to the object
					 */
					result.setDGAname(url);
					result.setTimestamp(timestamp);
					result.setZone(zone);

					/**
					 * We add the object to the arraylist to build the JSON file
					 */
					jsonResponse.add(result);
				} else {
					count = 0;
					LOGGER.info(url + " NOT found in the DGA database");
					LOGGER.info("Corrisponding CEF: " + cef);
					LOGGER.info("count: " + count);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setDGAname(e.getMessage());
			jsonResponse.add(result);
			return jsonResponse;
		}

		if (jsonResponse.isEmpty()) {
			result.setDGAname("Non è stato trovato Nessun DGA");
			jsonResponse.add(result);
		}

		return jsonResponse;
	}

}

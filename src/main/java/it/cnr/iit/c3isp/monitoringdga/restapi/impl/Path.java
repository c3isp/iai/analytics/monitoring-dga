package it.cnr.iit.c3isp.monitoringdga.restapi.impl;

public class Path {
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}

package it.cnr.iit.c3isp.monitordga.async;

import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import it.cnr.iit.c3isp.monitoringdga.restapi.impl.DGAServiceImplementation;
import it.cnr.iit.common.analytic.AbstractAnalyticExecutor;

@Service
public class AsyncAnalyticExecution extends AbstractAnalyticExecutor {

	@Autowired
	private DGAServiceImplementation dgaServiceImplementation;

	public AsyncAnalyticExecution() {
		// TODO Auto-generated constructor stub
	}

	@Async
	@Override
	public Future<String> executeWithResult(Object... objects) {
		try {
			System.out.println("going to sleep for 30 sec...");
			Thread.sleep(30000);
			System.out.println("aweking!");
			String vdlPath = (String) objects[0];
			String analytic = (String) objects[1];
			String outputFile = "";
			if (analytic.contentEquals("detectDga")) {
				outputFile = dgaServiceImplementation.detectDGA(vdlPath);
			} else {
				outputFile = dgaServiceImplementation.matchDGA(vdlPath);
			}
			return new AsyncResult<String>(outputFile);
		} catch (InterruptedException e) {
			e.printStackTrace();
			return new AsyncResult<String>("ERROR: " + e.getMessage());
		}
	}

	@Override
	protected <T> T executeWithResult() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void execute(Object... objects) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void execute() {
		// TODO Auto-generated method stub

	}

}
